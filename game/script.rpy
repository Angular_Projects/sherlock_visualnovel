﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define sherlock = Character("Sherlock")
define colin = Character("Colin")
define Amy = Character("Amy")


# The game starts here.

label start:

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene bg washington
    pause

    # Start the background music playing.
    # play music "audio/sound35.ogg"
    
    scene day03_16 with dissolve
    pause
    show path1cols with dissolve
    pause

    show path1cols:
         xalign 0.0
         linear 1.0 xalign 1.0
         linear 1.0 xalign 0.0
         repeat

    colin "Hello sherlock"
    play music "audio/sound36.ogg"

    scene day03_17 with dissolve
    Amy "Hey sherli..."
    pause

    scene day03_18:
          xalign 1.0 yalign 0.0
          linear 2.0 align (0.5, 1.0) knot (0.0, .33) knot (1.0, .66)
    pause

    scene day03_19 with dissolve
    pause

    show path1cols:
         xalign 1.0 yalign 0.0
         linear 2.0 align (0.5, 1.0) knot (0.0, .33) knot (1.0, .66)
        
    pause

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "eileen happy.png" to the images
    # directory.


    # These display lines of dialogue.

    sherlock "Hello Colin.. How is your PD going on.."

    Amy "Hi guys .. Nice to meet you both here ..."

    sherlock "Amy! you look cute.."

    # This ends the game.

    return
